require 'spec_helper'
require 'replicator/application'

module Replicator
  describe Application do
    let :with_attributes do
      {
        name: 'testing',
        api_key: 'lester-tester-12345',
        git_repo: 'git://github.com/diaspora/diaspora.git'
      }
    end
    subject { Replicator::Application.new with_attributes }

    it "initializes with given attributes" do
      subject.attributes.should == with_attributes
    end

    it "sets certain attributes to methods" do
      subject.git_repo.should == with_attributes[:git_repo]
      subject.name.should == with_attributes[:name]
    end

    it "computes a heroku app URL" do
      subject.repo_url.should == "http://#{with_attributes[:name]}.herokuapp.com"
    end

    it "computes a repo path on the local machine" do
      subject.repo_path.should == "/tmp/replicants/#{with_attributes[:git_repo]}"
    end

    context "when deploying to heroku" do
      it "computes the attributes necessary for Heroku::Command" do
        subject.heroku_attributes.should == {
          app: with_attributes[:name],
          region: with_attributes[:region],
          stack: with_attributes[:stack],
          tier: with_attributes[:tier],
          no_remote: true
        }
      end

      it "clones the repo at the computed repo path" do
        subject.send(:clone_repository).should be_true
        Dir.exists?("#{subject.repo_path}/.git").should be_true
      end

      it "pushes the cloned repo to heroku" do
        subject.send(:push_to_heroku).should be_true
      end

      it "removes the repo at the computed path" do
        subject.send(:remove_repository).should be_true
        Dir.exists?("#{subject.repo_path}/.git").should be_false
      end

      it "does not continue when steps fail" do
        subject.mock(:clone_repository) { false }

        subject.deploy!.should be_false
      end
    end
  end
end
