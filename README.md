# Replicator

Replicator is a library for quickly provisioning and deploying Heroku
apps.

## Installation

Add this line to your library's Gemfile:

    gem 'replicator'

Or if you're in a gemspec:

    spec.add_dependency 'replicator'

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install replicator

## Usage

Replicator can be used inside of another gem (for example, to add
project-specific configuration to the install process) or used directly
with the `replicate` command.

### Command Line

To use Replicator's reference command-line implementation:

    $ replicate APP_NAME --api-key=HEROKU_API_KEY --git-repo=GIT_REPO_URL

You will be prompted for your Heroku API key and Git repo if you do not
specify it in the options or the shell variables (they are the same
names as the examples given above, $HEROKU_API_KEY`, etc.). Although
this can be viewed as an example Replicator tool, it's actually useful
for building any Heroku app from a Git repo your machine has access to.

You can also specify other Heroku-related options here, such as region
and app stack.

### As a Library

Replicator can be used for creating your own command-line or web-based
tools for Heroku provisioning. Check the documentation for more info on
how to use Replicator as your command-line library for Heroku
provisioning.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
