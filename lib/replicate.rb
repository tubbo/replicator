require 'replicator'
require 'thor'

class Replicate < Thor
  include Thor::Actions

  desc 'install APP_NAME', "Provision the app at the given Git repo for Heroku"
  method_option :api_key, default: ENV['HEROKU_API_KEY']
  method_option :git_repo, default: ENV['SOURCE_GIT_REPO']
  method_option :region, default: 'us-east'
  method_option :stack, default: 'cedar'
  method_option :tier, default: 'free'
  def install app_name
    options = method_options
    replicant = Replicator.create \
      name: app_name,
      git_repo: options[:git_repo],
      region: options[:region],
      stack: options[:stack],
      tier: options[:tier]

    replicant.deploy!

    if replicant.deployed?
      say "Your replicant is available at '#{replicant.url}'"
    else
      say "There was an error deploying your replicant."
      exit 1
    end
  end
end
