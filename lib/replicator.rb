require "replicator/version"
require "replicator/application"

module Replicator
  def self.create from_attributes
    Application.new from_attributes
  end
end
