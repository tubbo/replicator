require 'heroku-api'

module Replicator
  class Application
    attr_reader :attributes, :git_repo, :name


    def initialize options
      @attributes = options
      %w(git_repo name).each do |reader|
        instance_variable_set "@#{reader}", options[reader.to_sym]
      end
      sh "mkdir -p /tmp/replicants" unless Dir.exists? "/tmp/replicants"
    end

    def url
      "http://#{name}.herokuapp.com"
    end

    def deploy!
      create_application and
      clone_repository and
      push_to_heroku and
      delete_repository
    end

    def deployed?
      true
    end

    def repo_path
      "/tmp/replicants/#{folder_name}"
    end

    def folder_name
      if git_repo =~ /\/\w\.git/
        $1
      else
        raise "Not a git repo: #{git_repo}"
      end
    end

    def git_repo
      attributes[:git_repo]
    end

  private
    def create_application
      heroku.post_app(heroku_attributes).status.to_i == 200
    end

    def heroku
      @client ||= Heroku::API.new api_key: attributes[:api_key]
    end

    def heroku_attributes
      {
        app: name,
        region: attributes[:region],
        stack: attributes[:stack],
        tier: attributes[:tier],
        no_remote: true
      }
    end

    def clone_repository
      sh "git clone #{git_repo} #{repo_path}"
    end

    def push_to_heroku
      sh "cd #{repo_path} && git push -u origin master"
    end

    def delete_repository
      sh "rm #{repo_path}"
    end

    def sh command
      result = system command
      result || false
    end
  end
end
